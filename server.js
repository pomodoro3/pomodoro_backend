const express = require('express');
const cors = require('cors');
const dotenv = require('dotenv');
const healthCheckRoutes = require('./src/routes/healthcheck');
const seance_router = require("./src/routes/seance_route");
const swaggerUi = require('swagger-ui-express');
const swaggerJsdoc = require('swagger-jsdoc');

dotenv.config();

const app = express();
const port = process.env.SERVICE_PORT || 3000;

app.use(express.json());
app.use(cors());

const swaggerOptions = {
    definition: {
        openapi: "3.0.0",
        info: {
            title: "Pomodoro API",
            version: "1.0.0",
            description: "API documentation for the Pomodoro project"
        },
        servers: [
            {
                url: `http://localhost:${port}`
            }
        ]
    },
    apis: ["./src/routes/*.js"],
};

const swaggerDocs = swaggerJsdoc(swaggerOptions);
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocs));

app.use('/healthcheck', healthCheckRoutes);

app.get("/", (req, res) => {
    res.send("Hello world");
});

app.use("/seance", seance_router);

app.listen(port, () => {
    const apiDocsUrl = `http://localhost:${port}/api-docs`;
    console.log(`Server is listening on port ${port}`);
    console.log(`API documentation is available at ${apiDocsUrl}`);
});
