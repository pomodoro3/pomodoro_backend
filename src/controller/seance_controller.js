const pool = require('../../database.js')
const queries = require('../model/seance_model.js')

/**
 * Crée une nouvelle séance.
 * @async
 * @param {Object} req - Requête HTTP.
 * @param {Object} res - Réponse HTTP.
 * @returns {Promise<void>}
 */
const createSeance = async (req, res) => {
    console.log('je suis dans le controllers')
    try {
        const {type, numberSession} = req.body

        await pool.query(queries.createSeance, [type, numberSession])

        res.status(201).send("Séance créé avec succès")
    } catch (error) {
        res.status(500).send({"error": error.message})
    }
}

/**
 * Met à jour le nombre de sessions d'une séance.
 * @param {Object} req - Requête HTTP.
 * @param {Object} res - Réponse HTTP.
 */
const updateBySeanceId = (req, res) => {
    const {numberSession} = req.body

    pool.query(queries.updateBySeanceId, [numberSession, req.params.id], (error, results) => {
        if (error) {
            res.status(400).json({"error":error.message})
            return
        }
        res.status(201).send("Le nombre de session a été modifié avec succès")
    })
}

module.exports = { 
    createSeance,
    updateBySeanceId
}
