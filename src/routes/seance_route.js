const { Router } = require('express');
const service = require('../controller/seance_controller.js');

const router = Router();

/**
 * @swagger
 * components:
 *   schemas:
 *     Seance:
 *       type: object
 *       required:
 *         - type
 *         - numberSession
 *       properties:
 *         type:
 *           type: string
 *           description: Type of the seance
 *         numberSession:
 *           type: integer
 *           description: Number of sessions
 */

/**
 * @swagger
 * /seance/create:
 *   post:
 *     summary: Create a new seance
 *     tags: [Seance]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/Seance'
 *     responses:
 *       201:
 *         description: Seance created successfully
 *       500:
 *         description: Server error
 */

/**
 * Route pour la création d'une nouvelle séance.
 * @name POST /seance/create
 * @function
 * @memberof module:routers/seanceRouter
 * @inner
 * @param {Object} req - Requête HTTP.
 * @param {Object} res - Réponse HTTP.
 */
router.post("/create", service.createSeance);

/**
 * @swagger
 * /seance/{id}:
 *   put:
 *     summary: Update a seance by ID
 *     tags: [Seance]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: ID of the seance to update
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               numberSession:
 *                 type: integer
 *     responses:
 *       201:
 *         description: Seance updated successfully
 *       400:
 *         description: Invalid input
 */

/**
 * Route pour la mise à jour d'une séance par ID.
 * @name PUT /seance/:id
 * @function
 * @memberof module:routers/seanceRouter
 * @inner
 * @param {Object} req - Requête HTTP.
 * @param {Object} res - Réponse HTTP.
 * @param {string} req.params.id - ID de la séance à mettre à jour.
 */
router.put("/:id", service.updateBySeanceId);

module.exports = router;
