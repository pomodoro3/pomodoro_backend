const createSeance = "INSERT INTO seance (type, numberSession) VALUES ($1, $2)"
const updateBySeanceId = "UPDATE seance SET numberSession = $1 WHERE id = $2"

module.exports = {
    createSeance,
    updateBySeanceId
}