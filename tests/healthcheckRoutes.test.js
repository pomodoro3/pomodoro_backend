const request = require('supertest');
const express = require('express');
const pool = require('../database');
const router = require('../src/routes/healthcheck');

const app = express();
app.use(express.json());
app.use('/', router);

describe('API Routes', () => {
    afterAll(async () => {
        await pool.end();
    });

    describe('GET /', () => {
        it('should return API online message', async () => {
            const response = await request(app).get('/');

            expect(response.status).toBe(200);
            expect(response.body.message).toBe('API en ligne.');
        });
    });
});