const request = require('supertest')
const express = require('express')
const router = require('../src/routes/seance_route')
const service = require('../src/controller/seance_controller')

jest.mock('../src/controller/seance_controller');

const app = express()
app.use(express.json())
app.use('/seance', router);

/**
 * Teste l'appel au controller createSeance.
 * @function
 * @name createSeance
 * @memberof module:tests/seanceCreateTest
 * @inner
 */
describe('Seance Router', () => {
    describe('POST /seance/create', () => {
        /**
         * Teste l'appel au service pour créer une séance avec succès.
         * @async
         * @function
         * @name shouldCreateSeanceSuccessfully
         * @memberof module:tests/seanceCreateTest
         * @inner
         */
        beforeEach(() => {
            service.createSeance.mockClear();
        });

        it('should call createSeance controller and return 201 on success', async () => {
            service.createSeance.mockImplementation((req, res) => {
                res.status(201).send('Séance créée avec succès');
            });

            const newSeance = {
                type: 'Yoga',
                numberSession: 5
            };

            const response = await request(app)
                .post('/seance/create')
                .send(newSeance);

            expect(response.status).toBe(201);
            expect(response.text).toBe('Séance créée avec succès');
            expect(service.createSeance).toHaveBeenCalledTimes(1);
        });

        /**
         * Teste le retour d'une erreur 500 lors de l'appel au controller.
         * @async
         * @function
         * @name shouldReturn500ErrorOnCallError
         * @memberof module:tests/seanceCreateTest
         * @inner
         */
        it('should handle error in createSeance controller and return 500 on failure', async () => {
            service.createSeance.mockImplementation((req, res) => {
                res.status(500).json({ error: 'Database error' });
            });

            const newSeance = {
                type: 'Yoga',
                numberSession: 5
            };

            const response = await request(app)
                .post('/seance/create')
                .send(newSeance);

            expect(response.status).toBe(500);
            expect(response.body.error).toBe('Database error');
            expect(service.createSeance).toHaveBeenCalledTimes(1);
        });
    });
});

/**
 * Teste l'appel au controller updateBySeanceId.
 * @function
 * @name updateBySeanceId
 * @memberof module:tests/seanceControllerTest
 * @inner
 */
describe('Seance Router', () => {
    describe('PUT /seance/:id', () => {
        /**
         * Teste l'appel au service pour mettre à jour une séance'
         * @async
         * @function
         * @name shouldUpdateSeanceSuccessfully
         * @memberof module:tests/seanceControllerTest.updateBySeanceId
         * @inner
         */
        beforeEach(() => {
            service.updateBySeanceId.mockClear()
        });

        it('should call updateBySeanceId controller and return 201 on success', async () => {
            service.updateBySeanceId.mockImplementation((req, res) => {
                res.status(201).send('Le nombre de session a été modifié avec succès');
            });

            const response = await request(app)
                .put('/seance/1')
                .send({ numberSession: 10 });

            expect(response.status).toBe(201);
            expect(response.text).toBe('Le nombre de session a été modifié avec succès');
            expect(service.updateBySeanceId).toHaveBeenCalledTimes(1);
        });

        /**
         * Teste le retour d'une erreur 500 lors de l'appel au controller.
         * @async
         * @function
         * @name shouldReturn500ErrorOnCallError
         * @memberof module:tests/seanceControllerTest.updateBySeanceId
         * @inner
         */
        it('should handle error in updateBySeanceId controller and return 400 on failure', async () => {
            service.updateBySeanceId.mockImplementation((req, res) => {
                res.status(400).json({ error: 'Database error' });
            });

            const response = await request(app)
                .put('/seance/1')
                .send({ numberSession: 10 });

            expect(response.status).toBe(400);
            expect(response.body.error).toBe('Database error');
            expect(service.updateBySeanceId).toHaveBeenCalledTimes(1);
        });
    });
});

