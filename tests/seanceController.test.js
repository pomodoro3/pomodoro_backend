const request = require('supertest');
const express = require('express');
const { createSeance, updateBySeanceId } = require('../src/controller/seance_controller');
const pool = require('../database');
const queries = require('../src/model/seance_model');

jest.mock('../database');
jest.mock('../src/model/seance_model');

const app = express();
app.use(express.json());
app.post('/createSeance', createSeance);
app.put('/updateSeance/:id', updateBySeanceId);

/**
 * Suite de tests pour le contrôleur des séances.
 * @memberof module:tests/seanceControllerTest
 * @inner
 */
describe('Seance Controller', () => {

    /**
     * Teste la fonction createSeance.
     * @function
     * @name createSeance
     * @memberof module:tests/seanceControllerTest
     * @inner
     */
    describe('createSeance', () => {

        /**
         * Teste la création d'une séance avec succès.
         * @async
         * @function
         * @name shouldCreateSeanceSuccessfully
         * @memberof module:tests/seanceControllerTest.createSeance
         * @inner
         */
        it('should create a seance successfully', async () => {
            const mockQuery = jest.fn().mockResolvedValue({});
            pool.query = mockQuery;
            queries.createSeance = 'INSERT INTO seance (type, numberSession) VALUES (?, ?)';

            const response = await request(app)
                .post('/createSeance')
                .send({ type: 'Yoga', numberSession: 5 });

            expect(response.status).toBe(201);
            expect(response.text).toBe('Séance créé avec succès');
            expect(mockQuery).toHaveBeenCalledWith('INSERT INTO seance (type, numberSession) VALUES (?, ?)', ['Yoga', 5]);
        });

        /**
         * Teste le retour d'une erreur 500 en cas d'erreur de base de données.
         * @async
         * @function
         * @name shouldReturn500ErrorOnDatabaseError
         * @memberof module:tests/seanceControllerTest.createSeance
         * @inner
         */
        it('should return a 500 error if there is a database error', async () => {
            const mockQuery = jest.fn().mockRejectedValue(new Error('Database error'));
            pool.query = mockQuery;
            queries.createSeance = 'INSERT INTO seance (type, numberSession) VALUES (?, ?)';

            const response = await request(app)
                .post('/createSeance')
                .send({ type: 'Yoga', numberSession: 5 });

            expect(response.status).toBe(500);
            expect(response.body.error).toBe('Database error');
        });
    });

    /**
     * Teste la fonction updateBySeanceId.
     * @function
     * @name updateBySeanceId
     * @memberof module:tests/seanceControllerTest
     * @inner
     */
    describe('updateBySeanceId', () => {

        /**
         * Teste la mise à jour d'une séance avec succès.
         * @async
         * @function
         * @name shouldUpdateSeanceSuccessfully
         * @memberof module:tests/seanceControllerTest.updateBySeanceId
         * @inner
         */
        it('should update a seance successfully', async () => {
            const mockQuery = jest.fn((query, values, callback) => callback(null, {}));
            pool.query = mockQuery;
            queries.updateBySeanceId = 'UPDATE seance SET numberSession = ? WHERE id = ?';

            const response = await request(app)
                .put('/updateSeance/1')
                .send({ numberSession: 10 });

            expect(response.status).toBe(201);
            expect(response.text).toBe('Le nombre de session a été modifié avec succès');
            expect(mockQuery).toHaveBeenCalledWith('UPDATE seance SET numberSession = ? WHERE id = ?', [10, '1'], expect.any(Function));
        });

        /**
         * Teste le retour d'une erreur 400 en cas d'erreur de base de données.
         * @async
         * @function
         * @name shouldReturn400ErrorOnDatabaseError
         * @memberof module:tests/seanceControllerTest.updateBySeanceId
         * @inner
         */
        it('should return a 400 error if there is a database error', async () => {
            const mockQuery = jest.fn((query, values, callback) => callback(new Error('Database error'), null));
            pool.query = mockQuery;
            queries.updateBySeanceId = 'UPDATE seance SET numberSession = ? WHERE id = ?';

            const response = await request(app)
                .put('/updateSeance/1')
                .send({ numberSession: 10 });

            expect(response.status).toBe(400);
            expect(response.body.error).toBe('Database error');
        });
    });
});
