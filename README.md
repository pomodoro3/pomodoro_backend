# Application Pomodoro

## Description

L'application Pomodoro est un mini projet backend en Express.js. Ce projet est conçu pour aider à gérer le temps en utilisant la technique Pomodoro. Le backend est construit avec Node.js et comprend une base de données PostgreSQL.

## Prérequis

Avant de commencer, assurez-vous d'avoir les éléments suivants :

- Node.js (v16.x.x ou supérieur)
- npm (v8.x.x ou supérieur)
- PostgreSQL (v14.x.x ou supérieur)

## Installation

1. Clonez le dépôt :

    ```bash
    git clone https://gitlab.com/pomodoro3/pomodoro_backend.git
    ```

2. Allez dans le répertoire du projet :

    ```bash
    cd pomodoro_backend
    ```

3. Installez les dépendances du backend :

    ```bash
    npm install
    ```

4. Configurez les variables d'environnement. Créez un fichier `.env` à la racine du répertoire et ajoutez les lignes suivantes :

    ```env
    POSTGRESQL_ADDON_HOST=""
    POSTGRESQL_ADDON_DATABASE=""
    POSTGRESQL_ADDON_USER=""
    POSTGRESQL_ADDON_PASSWORD=""
    POSTGRESQL_ADDON_PORT=""
    ```

## Démarrage du Projet

1. Démarrez le serveur backend :

    ```bash
    npm start
    ```

2. Pour le mode développement avec rechargement à chaud :

    ```bash
    npm run dev
    ```

3. Exécutez les tests :

    ```bash
    npm test
    ```

## Documentation de l'API

La documentation de l'API est générée à l'aide de Swagger. Pour accéder à la documentation de l'API, démarrez le serveur backend et rendez-vous sur `http://localhost:3000/api-docs` dans votre navigateur.

## Points de Terminaison de l'API

- **GET /healthcheck**: Vérifie l'état de santé de l'application.
- **GET /seance**: Récupère toutes les séances.
- **POST /seance**: Crée une nouvelle séance.
- **GET /**: Page d'accueil de l'API.

## Documentation automatique de l'API

#### Commande permettant de générer une documentation

    ```bash 
        jsdoc -c jsdoc.json
    ```

