const scanner = require('sonarqube-scanner').default;

scanner(
  {
    serverUrl: 'http://localhost:9000',
    options : {
      'sonar.projectKey': 'pomodoroBack',
      'sonar.projectName': 'pomodoroBack',
      'sonar.sources': 'src',
      'sonar.login': 'squ_fa4cc3b326c3aad4acfd5fa0ed8de3a84aa520df'
    }
  },
  () => process.exit()
);
