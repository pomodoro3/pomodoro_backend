module.exports = {
    // Autres configurations de Jest
    collectCoverage: true,
    coverageDirectory: 'coverage',
    coverageReporters: ['lcov', 'text-summary'],
    // reporters: [
    //   'default',
    //   ['jest-sonar-reporter', {
    //     reportPath: 'report',
    //     reportFile: 'test-report.xml',
    //     indent: 4,
    //   }]
    // ],
  };
  